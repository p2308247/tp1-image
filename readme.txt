Projet réalisé par Temirboulatov Koureich p1707160 et Herda Abdelaziz p2308247.
Dans le cadre de l'UE Informatique Graphique Et Image . 2023-2024 .
Responsable Mme BOUAKAZ BRONDEL SAIDA Université Claude Bernard Lyon 1 (UCBL).


Le projet propose une implémenation de la segmentation d'image par la méthode de croissance de région (region growing) en c++ et avec opencv.

# Prérequis

Assurez d'avoir opencv et un compilateur c++ installés et configurés sur votre machine.

# Pour compiler

Depuis la racine du projet : 

cd build/

make

# Execution

sudo ./main 