#include <iostream>
#include <opencv2/opencv.hpp>
#include <functional>
#include <math.h>

using namespace cv;
using namespace std;

const double meanOfImage(const string imagePath)
{

    // Load the image
    Mat image = imread(imagePath, IMREAD_COLOR);

    if (image.empty())
    {
        cerr << "Error: Image cannot be loaded!" << endl;
        return -1;
    }

    // Calculate the mean of the image
    Scalar meanValue = mean(image);

    // Display the mean values for each channel
    cout << "Mean intensity in B channel: " << meanValue[0] << endl;
    cout << "Mean intensity in G channel: " << meanValue[1] << endl;
    cout << "Mean intensity in R channel: " << meanValue[2] << endl;

    // If you want to calculate the overall average intensity
    double overallMean = (meanValue[0] + meanValue[1] + meanValue[2]) / 3.0;

    return overallMean;
}

const double ecartTypeImage(const string &imagePath, const double Callback(const string))
{
    double imageMean = Callback(imagePath);
    return sqrt(imageMean);
}

vector<int> histogram(const string &imagePath)
{
    // Load the image in grayscale
    Mat image = imread(imagePath, IMREAD_GRAYSCALE);

    // Calculate histogram
    vector<int> histogram(256, 0);

    for (int y = 0; y < image.rows; y++)
    {
        for (int x = 0; x < image.cols; x++)
        {
            histogram[(int)image.at<uchar>(y, x)]++;
        }
    }

    return histogram;
}

const void histogram_normalised(const vector<int> &histogram, const string &imagePath)
{

    Mat image = imread(imagePath, IMREAD_GRAYSCALE);

    // Normalize the histogram to get probabilities
    vector<float> normalizedHistogram(256);
    int totalPixels = image.rows * image.cols;
    for (int i = 0; i < 256; i++)
    {
        normalizedHistogram[i] = (float)histogram[i] / totalPixels;
    }

    // Create an image to display the normalized histogram
    int hist_w = 512; // Width of the histogram image
    int hist_h = 400; // Height of the histogram image
    int bin_w = cvRound((double)hist_w / 256);
    Mat histImage(hist_h, hist_w, CV_8UC1, Scalar(0));

    // Find the maximum probability for scaling the histogram image
    float maxProb = *max_element(normalizedHistogram.begin(), normalizedHistogram.end());

    // Scale and draw the histogram
    for (int i = 0; i < 256; i++)
    {
        float binHeight = normalizedHistogram[i] / maxProb * hist_h; // Scale bin value
        line(histImage, Point(bin_w * i, hist_h),
             Point(bin_w * i, hist_h - binHeight),
             Scalar(255), 2, 8, 0);
    }

    // Display the normalized histogram
    namedWindow("Normalized Histogram", WINDOW_AUTOSIZE);
    imshow("Normalized Histogram", histImage);
}

const void histogram_streched(const string &imagePath)
{

    // Chargez l'image en niveaux de gris
    Mat image = imread(imagePath, IMREAD_GRAYSCALE);

    // Trouvez les intensités minimales et maximales dans l'image
    double minVal, maxVal;
    minMaxLoc(image, &minVal, &maxVal);

    // Étirez l'histogramme
    Mat stretchedImage;
    // Formule pour l'étirement : ((image - minVal) * (b - a) / (maxVal - minVal)) + a
    // où [a, b] est la nouvelle plage d'intensité que nous voulons utiliser, typiquement [0, 255]
    image.convertTo(stretchedImage, CV_8U, 255.0 / (maxVal - minVal), -minVal * 255.0 / (maxVal - minVal));

    // Affichez l'image originale et l'image étirée
    namedWindow("Image Originale", WINDOW_AUTOSIZE);
    imshow("Image Originale", image);

    namedWindow("Image Etiree", WINDOW_AUTOSIZE);
    imshow("Image Etiree", stretchedImage);
}

const void histogram_egalized(const string &imagePath)
{
    // Chargez l'image en niveaux de gris
    Mat image = imread(imagePath, IMREAD_GRAYSCALE);

    // Créez un objet Mat pour stocker l'image égalisée
    Mat equalizedImage;

    // Appliquez l'égalisation de l'histogramme
    equalizeHist(image, equalizedImage);

    // Affichez l'image originale
    namedWindow("Image Originale", WINDOW_AUTOSIZE);
    imshow("Image Originale", image);

    // Affichez l'image après égalisation de l'histogramme
    namedWindow("Image Egalisee", WINDOW_AUTOSIZE);
    imshow("Image Egalisee", equalizedImage);
}

const void histogram_print(const vector<int> &Histo)
{

    vector<int> Histogram = Histo;

    // Create an image to display the histogram
    int hist_w = 512; // Width of the histogram image
    int hist_h = 400; // Height of the histogram image
    int bin_w = cvRound((double)hist_w / 256);

    Mat histImage(hist_h, hist_w, CV_8UC1, Scalar(0));

    // Find the maximum intensity element from histogram
    int max = Histogram[0];
    for (int i = 1; i < 256; i++)
    {
        if (max < Histogram[i])
        {
            max = Histogram[i];
        }
    }

    // Normalize the histogram to fit the histImage height
    for (int i = 0; i < 256; i++)
    {
        Histogram[i] = ((double)Histogram[i] / max) * hist_h;
    }

    // Draw the intensity line for histogram
    for (int i = 0; i < 256; i++)
    {
        line(histImage, Point(bin_w * (i), hist_h),
             Point(bin_w * (i), hist_h - Histogram[i]),
             Scalar(255), 2, 8, 0);
    }

    // Display the histogram
    namedWindow("Intensity Histogram", WINDOW_AUTOSIZE);
    imshow("Intensity Histogram", histImage);
}

Mat applyFilter(const Mat &inputImage, const Mat &kernel)
{
    // Creation d'une image de sortie
    Mat outputImage = Mat::zeros(inputImage.size(), inputImage.type());

    // Ajouter un padding a l'image d'entree
    int pad = kernel.rows / 2;
    Mat paddedInput;
    copyMakeBorder(inputImage, paddedInput, pad, pad, pad, pad, BORDER_REPLICATE);

    // Operation de convolution
    for (int y = pad; y < paddedInput.rows - pad; ++y)
    {
        for (int x = pad; x < paddedInput.cols - pad; ++x)
        {
            double sum = 0.0;
            for (int k = -pad; k <= pad; ++k)
            {
                for (int j = -pad; j <= pad; ++j)
                {
                    sum += kernel.at<float>(k + pad, j + pad) * paddedInput.at<uchar>(y + k, x + j);
                }
            }
            outputImage.at<uchar>(y - pad, x - pad) = saturate_cast<uchar>(sum);
        }
    }

    return outputImage;
}

const void convoluted_image(const string &imagePath)
{
    // Charger l'image
    Mat image = imread(imagePath, IMREAD_GRAYSCALE);

    // Convolution kernel (example: 3x3 filtre gaussien)
    Mat kernel = (Mat_<float>(3, 3) << 1 / 16.0, 2 / 16.0, 1 / 16.0,
                  2 / 16.0, 4 / 16.0, 2 / 16.0,
                  1 / 16.0, 2 / 16.0, 1 / 16.0);

    // Appliquer le filtre sur l'image
    Mat filteredImage = applyFilter(image, kernel);

    // Afficher l'image originale et l'image filtree
    namedWindow("Filtered Image", WINDOW_AUTOSIZE);
    imshow("Original Image", image);
    imshow("Filtered Image", filteredImage);
}

int main()
{
    string image = "../Lenna.png";
    double moyenne_image = meanOfImage(image);
    double ecart_type_image = ecartTypeImage(image, meanOfImage);
    cout << "Moyenne de l'image : " << moyenne_image << endl;
    cout << "Ecart type de l'image : " << ecart_type_image << endl;

    cout << endl;

    vector<int> histo = histogram(image);
    histogram_print(histo);
    histogram_normalised(histo, image);
    histogram_streched(image);
    histogram_egalized(image);
    convoluted_image(image);

    waitKey(0);
    return 0;
}
