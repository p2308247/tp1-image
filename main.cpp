#include <iostream>
#include <vector>
#include <opencv2/opencv.hpp>
#include <time.h>
#include <random>
#include <thread>
#include <mutex>

using namespace std;
using namespace cv;

void placer_germes(int nb_germes, const Mat &src, vector<Point> &germes)
{
    for (int i = 0; i < nb_germes; ++i)
    {
        int x = rand() % src.cols;
        int y = rand() % src.rows;
        Point g = {x, y};
        germes.push_back(g);
    }
}

bool est_pixel_valide(const Vec3b &couleur_germe, const Vec3b &couleur_cible, int seuil)
{
    return abs(couleur_germe[0] - couleur_cible[0]) < seuil &&
           abs(couleur_germe[1] - couleur_cible[1]) < seuil &&
           abs(couleur_germe[2] - couleur_cible[2]) < seuil;
}

bool est_pixel_valide2(const Vec3b &couleur_germe, const Vec3b &couleur_cible, int seuil)
{
    return norm(couleur_germe - couleur_cible) < seuil;
}

/*
    Cette fonction effectue la croissance des régions sur une image en couleur
    On effectue la croissance des régions autour des germes. On ajoute les pixels voisins qui sont valides
    dans la région. Un pixel est valide s'il est proche de la couleur du germe. La distance entre la couleur
    du germe et la couleur du pixel doit être inférieure à un seuil.
    On attribue une couleur aléatoire à chaque région afin de les distinguer.
*/
Mat regionGrowing(const Mat &src, int seuil, const vector<Point> &germes)
{
    // Créer une image de destination, initialisée à zéro (noir)
    Mat dst = Mat::zeros(src.rows, src.cols, CV_8UC3);

    // Définir une couleur aléatoire pour chaque région
    vector<Vec3b> couleurs;
    for (size_t i = 0; i < germes.size(); ++i)
    {
        couleurs.push_back(Vec3b(rand() % 256, rand() % 256, rand() % 256));
    }

    // Parcourir tous les germes
    for (size_t i = 0; i < germes.size(); ++i)
    {
        // File d'attente pour la croissance de la région
        queue<Point> queue;
        queue.push(germes[i]);

        // Tant que la file n'est pas vide
        while (!queue.empty())
        {
            Point p = queue.front();
            queue.pop();

            // Vérifier si le pixel est déjà traité
            if (dst.at<Vec3b>(p) != Vec3b(0, 0, 0))
                continue;

            // Marquer le pixel dans l'image de destination
            dst.at<Vec3b>(p) = couleurs[i];

            // Vérifier les voisins
            for (int dx = -1; dx <= 1; ++dx)
            {
                for (int dy = -1; dy <= 1; ++dy)
                {
                    Point voisin = Point(p.x + dx, p.y + dy);

                    // Vérifier si le voisin est dans les limites de l'image
                    if (voisin.x >= 0 && voisin.x < src.cols && voisin.y >= 0 && voisin.y < src.rows)
                    {
                        // Vérifier si le pixel voisin est similaire au germe
                        if (est_pixel_valide(src.at<Vec3b>(germes[i]), src.at<Vec3b>(voisin), seuil))
                        {
                            queue.push(voisin);
                        }
                    }
                }
            }
        }
    }

    return dst;
}

/*
    Dans cette fonction presque identique à la fonction regionGrowing, on effectue une fusion des régions
    similaires. On calcule la couleur moyenne de chaque région et on fusionne les régions dont les couleurs
    moyennes sont proches.
*/
Mat regionGrowingFusion(const Mat &src, int seuil, const vector<Point> &germes, int seuilFusion)
{
    // Initialiser les images de destination
    Mat dst = Mat::zeros(src.rows, src.cols, CV_8UC3);
    Mat labels = Mat::zeros(src.rows, src.cols, CV_32S); // Image des labels des régions

    vector<Vec3b> couleurs;
    vector<Vec3f> couleursMoyennes; // Pour stocker les couleurs moyennes
    vector<int> tailleRegions;      // Pour stocker la taille de chaque région

    // Initialisation des couleurs aléatoires et des vecteurs
    for (size_t i = 0; i < germes.size(); ++i)
    {
        Vec3b couleur = Vec3b(rand() % 256, rand() % 256, rand() % 256);
        couleurs.push_back(couleur);
        couleursMoyennes.push_back(Vec3f(0, 0, 0));
        tailleRegions.push_back(0);
    }

    // Croissance des régions
    for (size_t i = 0; i < germes.size(); ++i)
    {
        queue<Point> queue;
        queue.push(germes[i]);

        while (!queue.empty())
        {
            Point p = queue.front();
            queue.pop();

            if (dst.at<Vec3b>(p) != Vec3b(0, 0, 0))
                continue;

            dst.at<Vec3b>(p) = couleurs[i];
            labels.at<int>(p) = i;

            couleursMoyennes[i] += Vec3f(src.at<Vec3b>(p));
            tailleRegions[i]++;

            for (int dx = -1; dx <= 1; ++dx)
            {
                for (int dy = -1; dy <= 1; ++dy)
                {
                    Point voisin = Point(p.x + dx, p.y + dy);
                    if (voisin.x >= 0 && voisin.x < src.cols && voisin.y >= 0 && voisin.y < src.rows)
                    {
                        if (est_pixel_valide(src.at<Vec3b>(germes[i]), src.at<Vec3b>(voisin), seuil))
                        {
                            queue.push(voisin);
                        }
                    }
                }
            }
        }
    }

    // Calcul des couleurs moyennes
    for (size_t i = 0; i < germes.size(); ++i)
    {
        couleursMoyennes[i] /= tailleRegions[i];
    }

    // Fusion des régions similaires
    for (int y = 0; y < labels.rows; ++y)
    {
        for (int x = 0; x < labels.cols; ++x)
        {
            int label = labels.at<int>(y, x);
            Vec3b couleurRegion = couleurs[label];

            for (int dy = -1; dy <= 1; ++dy)
            {
                for (int dx = -1; dx <= 1; ++dx)
                {
                    int ny = y + dy;
                    int nx = x + dx;
                    if (nx >= 0 && nx < labels.cols && ny >= 0 && ny < labels.rows)
                    {
                        int labelVoisin = labels.at<int>(ny, nx);
                        // Vérifier si les régions sont différentes et adjacentes
                        if (label != labelVoisin)
                        {
                            Vec3b couleurVoisin = couleurs[labelVoisin];
                            // Calculer la distance entre les couleurs moyennes
                            if (norm(couleursMoyennes[label] - couleursMoyennes[labelVoisin]) < seuilFusion)
                            {
                                // Fusionner les régions
                                labels.at<int>(ny, nx) = label;
                                dst.at<Vec3b>(ny, nx) = couleurRegion;
                            }
                        }
                    }
                }
            }
        }
    }
    return dst;
}

// Fonction qui affiche les frontieres des regions
Mat afficher_frontieres(const Mat &src)
{
    Mat dst = Mat::zeros(src.rows, src.cols, CV_8UC3);

    for (int y = 0; y < src.rows; ++y)
    {
        for (int x = 0; x < src.cols; ++x)
        {
            if (x > 0 && src.at<Vec3b>(y, x) != src.at<Vec3b>(y, x - 1))
            {
                dst.at<Vec3b>(y, x) = Vec3b(255, 255, 255);
            }
            else if (x < src.cols - 1 && src.at<Vec3b>(y, x) != src.at<Vec3b>(y, x + 1))
            {
                dst.at<Vec3b>(y, x) = Vec3b(255, 255, 255);
            }
            else if (y > 0 && src.at<Vec3b>(y, x) != src.at<Vec3b>(y - 1, x))
            {
                dst.at<Vec3b>(y, x) = Vec3b(255, 255, 255);
            }
            else if (y < src.rows - 1 && src.at<Vec3b>(y, x) != src.at<Vec3b>(y + 1, x))
            {
                dst.at<Vec3b>(y, x) = Vec3b(255, 255, 255);
            }
        }
    }

    return dst;
}

void increaseContrast(Mat &image, double alpha, int beta)
{
    // alpha: Facteur de contraste (1.0 = pas de changement)
    // beta: Valeur à ajouter pour ajuster la luminosité (0 = pas de changement)

    Mat newImage = Mat::zeros(image.size(), image.type());

    // Ajuster le contraste et la luminosité
    for (int y = 0; y < image.rows; y++)
    {
        for (int x = 0; x < image.cols; x++)
        {
            for (int c = 0; c < image.channels(); c++)
            {
                newImage.at<Vec3b>(y, x)[c] =
                    saturate_cast<uchar>(alpha * image.at<Vec3b>(y, x)[c] + beta);
            }
        }
    }

    image = newImage;
}

void increaseSaturation(Mat &image, double alpha)
{
    // alpha: Facteur de saturation (1.0 = pas de changement)

    Mat newImage = Mat::zeros(image.size(), image.type());

    // Ajuster la saturation
    for (int y = 0; y < image.rows; y++)
    {
        for (int x = 0; x < image.cols; x++)
        {
            Vec3b couleur = image.at<Vec3b>(y, x);
            int max = std::max(couleur[0], std::max(couleur[1], couleur[2]));
            int min = std::min(couleur[0], std::min(couleur[1], couleur[2]));
            int delta = max - min;
            if (delta == 0)
            {
                newImage.at<Vec3b>(y, x) = couleur;
            }
            else
            {
                double saturation = delta / (double)max;
                double value = max / 255.0;
                double C = value * saturation;
                double X = C * (1 - abs(fmod((double)max / 60.0, 2) - 1));
                double m = value - C;
                double r, g, b;
                if (max == couleur[0])
                {
                    r = C;
                    g = X;
                    b = 0;
                }
                else if (max == couleur[1])
                {
                    r = X;
                    g = C;
                    b = 0;
                }
                else
                {
                    r = 0;
                    g = X;
                    b = C;
                }
                newImage.at<Vec3b>(y, x) = Vec3b((r + m) * 255, (g + m) * 255, (b + m) * 255);
            }
        }
    }

    image = newImage;
}

int main()
{
    srand((unsigned int)time(NULL));

    // Charger une image
    Mat src = imread("../3.jpg");
    Mat src2 = imread("../2.jpg");

    // Vérifier si l'image a été chargée correctement
    if (src.empty() || src2.empty())
    {
        cerr << "Erreur: Impossible de charger l'image." << endl;
        return -1;
    }

    // Placer les germes
    vector<Point> germes;

    // Appliquer la croissance de région
    int seuil = 30; //
    int seuilFusion = 50;

    // flou gaussien
    GaussianBlur(src2, src2, Size(3, 3), 0, 0);
    GaussianBlur(src, src, Size(3, 3), 0, 0);
    // increaseSaturation(src2, 1.5);

    placer_germes(500, src, germes);
    Mat regiongrowing_src = regionGrowing(src, seuil, germes);
    Mat regiongrowing_fusion_src = regionGrowingFusion(src, seuil, germes, seuilFusion);
    Mat frontieres = afficher_frontieres(regiongrowing_src);

    germes.clear();
    placer_germes(500, src2, germes);
    Mat regiongrowing_src2 = regionGrowing(src2, seuil, germes);
    Mat regiongrowing_fusion_src2 = regionGrowingFusion(src2, seuil, germes, seuilFusion);
    Mat frontieres2 = afficher_frontieres(regiongrowing_src2);

    cout << germes.size() << endl;

    // Afficher les résultats
    imshow("Image originale src", src);
    imshow("Résultat Region Growing", regiongrowing_src);
    imshow("Résultat Region Growing Fusion", regiongrowing_fusion_src);
    imshow("Frontieres", frontieres);

    // imshow("Image Originale src2", src2);
    // imshow("Résultat Region Growing", regiongrowing_src2);
    // imshow("Résultat Region Growing Fusion", regiongrowing_fusion_src2);
    // imshow("Frontieres", frontieres2);

    waitKey(0);
    return 0;
}
